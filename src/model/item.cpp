/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Item
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#include "model/item.hpp"

std::ostream& Item::display(std::ostream& os) const {
    return os << "Item { name = "<< _name << ", price = " << _price << " }";
}
