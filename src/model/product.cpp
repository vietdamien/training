/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Product
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#include "model/product.hpp"

const double Product::default_price { 10 };

unsigned Product::_constructs {};
unsigned Product::_copies {};
unsigned Product::_transfers {};

Product::Product(std::string name, double price) : _name(std::move(name)), _price(price) {
    ++_constructs;
}

Product::Product(const Product& other) : Product(other._name, other._price) {
    ++_copies;
}

Product::Product(Product&& other) noexcept : Product(std::move(other._name), other._price) {
    ++_transfers;
}

unsigned Product::constructs() {
    return _constructs;
}

unsigned Product::copies() {
    return _copies;
}

unsigned Product::transfers() {
    return _transfers;
}

const std::string& Product::name() const {
    return _name;
}

double Product::price() const {
    return _price;
}

Product& Product::operator=(const Product& rhs) {
    if (this != &rhs) {
        _name = rhs._name;
        _price = rhs._price;
        ++_copies;
    }

    return *this;
}

Product& Product::operator=(Product&& rhs) {
    if (this != &rhs) {
        _name = std::move(rhs._name);
        _price = rhs._price;
        ++_transfers;
    }

    return *this;
}

std::ostream& operator<<(std::ostream& os, const Product& rhs) {
    return rhs.display(os);
}
