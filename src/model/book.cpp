/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Book
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#include "model/book.hpp"

Book::Book(std::string name, std::string author, double price) : Product(name, price), _author(std::move(author)) {

}

Book::Book(const Book& other) : Product(other), _author(other._author) {

}

Book::Book(Book&& other) noexcept: Product(std::move(other)), _author(std::move(other._author)) {

}

std::ostream& Book::display(std::ostream& os) const {
    return os << "Book { name = " << _name << ", author = " << _author << ", price = " << _price << " }";
}

const std::string& Book::author() const {
    return _author;
}

Book& Book::operator=(const Book& rhs) {
    if (this != &rhs) {
        _author = rhs._author;
        static_cast<Product&>(*this).operator=(static_cast<const Product&>(rhs));
    }

    return *this;
}

Book& Book::operator=(Book&& rhs) noexcept {
    if (this != &rhs) {
        _author = std::move(rhs._author);
        static_cast<Product&>(*this).operator=(std::move(rhs));
    }

    return *this;
}
