/* *********************************************************************************************************************
 * Project name: Training
 * File name   : PriceRange
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#include "model/price_range.hpp"

PriceRange::PriceRange(int inf, int sup) : _inf { inf }, _sup { sup } {

}

bool PriceRange::contains(double value) const {
    return value >= _inf && value <= _sup;
}

bool PriceRange::operator<(const PriceRange& rhs) const {
    return _inf < rhs._inf || _sup < rhs._sup;
}

bool PriceRange::operator>(const PriceRange& rhs) const {
    return rhs < *this;
}

bool PriceRange::operator<=(const PriceRange& rhs) const {
    return !(rhs < *this);
}

bool PriceRange::operator>=(const PriceRange& rhs) const {
    return !(*this < rhs);
}

bool PriceRange::operator==(const PriceRange& rhs) const {
    return _inf == rhs._inf &&
           _sup == rhs._sup;
}

bool PriceRange::operator!=(const PriceRange& rhs) const {
    return !(rhs == *this);
}

std::ostream& operator<<(std::ostream& os, const PriceRange& range) {
    return os << "[" << range._inf << ", " << range._sup << "]";
}
