/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Product
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#ifndef _TRAINING_PRODUCT_HPP_
#define _TRAINING_PRODUCT_HPP_

#include <ostream>
#include <string>

class Product {
protected:
    static const double default_price;

    static unsigned _constructs;
    static unsigned _copies;
    static unsigned _transfers;

    std::string _name;
    double _price;

public:
    Product(std::string name, double price = default_price);
    Product(const Product& other);
    Product(Product&& other) noexcept;

    static unsigned constructs();
    static unsigned copies();
    static unsigned transfers();

    const std::string& name() const;
    double price() const;

    virtual std::ostream& display(std::ostream& os) const = 0;

    Product& operator=(const Product& rhs);
    Product& operator=(Product&& rhs);

    friend std::ostream& operator<<(std::ostream& os, const Product& rhs);
};

#endif // _TRAINING_PRODUCT_HPP_
