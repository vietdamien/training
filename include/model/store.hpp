/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Store
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#ifndef _TRAINING_STORE_HPP_
#define _TRAINING_STORE_HPP_

#include <map>
#include <ostream>
#include <vector>

#include "price_range.hpp"

template<typename Tp>
class Store {
public:
    using ItemPtr = std::unique_ptr<Tp>;
    using ItemsVector = std::vector<ItemPtr>;
    using ItemsMap = std::multimap<PriceRange, ItemsVector>;

    std::size_t count_keys() const;
    std::size_t count_items() const;
    std::size_t count_items(const PriceRange& key) const;

    void add_key(PriceRange key);
    void add_item(std::unique_ptr<Tp> item);

    void remove_key(int inf, int sup);
    void remove_item(const std::string& name);

    template<typename Tp_>
    friend std::ostream& operator<<(std::ostream& os, const Store<Tp_>& rhs);

private:
    ItemsMap _items;
};

#include "model/store.inl"

#endif // _TRAINING_STORE_HPP_
