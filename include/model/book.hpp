/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Book
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#ifndef _TRAINING_BOOK_HPP_
#define _TRAINING_BOOK_HPP_

#include <ostream>
#include <string>

#include "model/product.hpp"

class Book : public Product {
private:
    std::string _author;

public:
    Book(std::string name, std::string author, double price = Product::default_price);
    Book(const Book& other);
    Book(Book&& other) noexcept;

    std::ostream& display(std::ostream& os) const override;

    const std::string& author() const;

    Book& operator=(const Book& rhs);
    Book& operator=(Book&& rhs) noexcept;
};

#endif // _TRAINING_BOOK_HPP_
