/* *********************************************************************************************************************
 * Project name: Training
 * File name   : PriceRangeGenerator
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#ifndef _TRAINING_PRICE_RANGE_GENERATOR_HPP_
#define _TRAINING_PRICE_RANGE_GENERATOR_HPP_

#include <vector>

template<int Step, int Start = 0, int Count = 1>
struct PriceRangeGenerator {
    std::vector<PriceRange> operator()();
};

#include "model/price_range_generator.inl"

#endif // _TRAINING_PRICE_RANGE_GENERATOR_HPP_
