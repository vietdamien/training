/* *********************************************************************************************************************
 * Project name: Training
 * File name   : PriceRange
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#ifndef _TRAINING_PRICE_RANGE_HPP_
#define _TRAINING_PRICE_RANGE_HPP_

#include <ostream>

class PriceRange {
private:
    int _inf;
    int _sup;

public:
    PriceRange(int inf = 0, int sup = 0);
    PriceRange(const PriceRange&) = default;
    PriceRange(PriceRange&&) = default;

    bool contains(double value) const;

    bool operator<(const PriceRange& rhs) const;
    bool operator>(const PriceRange& rhs) const;
    bool operator<=(const PriceRange& rhs) const;
    bool operator>=(const PriceRange& rhs) const;

    bool operator==(const PriceRange& rhs) const;
    bool operator!=(const PriceRange& rhs) const;

    PriceRange& operator=(const PriceRange&) = default;
    PriceRange& operator=(PriceRange&&) = default;

    friend std::ostream& operator<<(std::ostream& os, const PriceRange& range);
};

#endif // _TRAINING_PRICE_RANGE_HPP_
