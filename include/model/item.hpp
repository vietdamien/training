/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Item
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#ifndef _TRAINING_ITEM_HPP_
#define _TRAINING_ITEM_HPP_

#include "product.hpp"

class Item : public Product {
public:
    using Product::Product;

    std::ostream& display(std::ostream& os) const override;
};

#endif // _TRAINING_ITEM_HPP_
