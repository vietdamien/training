/* *********************************************************************************************************************
 * Project name: Training
 * File name   : Store
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

#include <stdexcept>

template<typename Tp>
std::size_t Store<Tp>::count_keys() const {
    return _items.size();
}

template<typename Tp>
std::size_t Store<Tp>::count_items() const {
    std::size_t count = 0;

    for (const auto& pair: _items) {
        count += pair.second.size();
    }

    return count;
}

template<typename Tp>
std::size_t Store<Tp>::count_items(const PriceRange& key) const {
    if (!_items.contains(key)) {
        throw std::exception();
    }

    return _items.find(key)->second.size();
}

template<typename Tp>
void Store<Tp>::add_key(PriceRange key) {
    _items.emplace(key, ItemsVector {});
}

template<typename Tp>
void Store<Tp>::add_item(std::unique_ptr<Tp> item) {
    auto it = std::find_if(_items.begin(), _items.end(), [&item](const auto& pair) {
        return pair.first.contains(item->price());
    });

    if (it == _items.end()) {
        return;
    }

    it->second.emplace_back(std::move(item));
}

template<typename Tp>
void Store<Tp>::remove_key(int inf, int sup) {
    auto range { _items.equal_range(PriceRange { inf, sup }) };
    _items.erase(range.first, range.second);
}

template<typename Tp>
void Store<Tp>::remove_item(const std::string& name) {
    for (auto& pair: _items) {
        const auto& begin = std::remove_if(pair.second.begin(), pair.second.end(), [&name](const auto& ptr) {
            return ptr->name() == name;
        });

        pair.second.erase(begin, pair.second.end());
    }
}

template<typename Tp_>
std::ostream& operator<<(std::ostream& os, const Store<Tp_>& rhs) {
    os << "{" << std::endl;

    for (const auto& pair: rhs._items) {
        os << "\t" << pair.first << std::endl;

        for (const auto& ptr: pair.second) {
            os << "\t\t" << *ptr << std::endl;
        }
    }

    return os << "}";
}
