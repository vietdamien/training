/* *********************************************************************************************************************
 * Project name: Training
 * File name   : PriceRangeGenerator
 * Author      : Damien Nguyen
 * Date        : Sunday, December 04, 2022 
 * ********************************************************************************************************************/

template<int Step, int Start, int Count>
std::vector<PriceRange> PriceRangeGenerator<Step, Start, Count>::operator()() {
    std::vector<PriceRange> result;

    for (int i = 0; i < Count; ++i) {
        result.emplace_back(i * Step + Start, (i + 1) * Step + Start - 1);
    }

    return result;
}
